package com.sdo.fakeapp.app;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;



public class App {
	
	private static Logger log = Logger.getLogger(App.class);
	
	public static void main(String[] args) throws InterruptedException, IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException, ClassNotFoundException {
		
		int i=0;
		
		while (i<10000) {
			Babar b = new Babar();
			log.info(++i+" Message : "+b.getMessage());
			Thread.sleep(2000);
		}
	}
	


}
