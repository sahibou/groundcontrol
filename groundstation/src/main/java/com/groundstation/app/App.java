package com.groundstation.app;

import com.groundstation.fileeventprocessor.FileEventProcessor;
import com.groundstation.filemonitor.FileUpdateMonitor;

public class App {
	
	public static void main(String[] args) throws Exception {
		//ClassUtils cu = new ClassUtils(args);
		
		FileUpdateMonitor mon = new FileUpdateMonitor();

		mon.addObserver(new FileEventProcessor(args));
		mon.run(args);
		
	}
}