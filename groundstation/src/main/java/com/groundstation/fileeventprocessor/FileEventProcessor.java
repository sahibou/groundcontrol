package com.groundstation.fileeventprocessor;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.Logger;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import com.groundstation.utils.MessageUtils;

public class FileEventProcessor implements Observer {

	private static final Logger log = Logger.getLogger(FileEventProcessor.class);
	private static final ZContext context = new ZContext();
	private static ZMQ.Socket socket;
	
	/**
	 * INIT : Load URLS INTO AGENT FROM COMMAND LINE ARGS
	 * @param args
	 */
	public FileEventProcessor(String[] args) {
		socket = context.createSocket(ZMQ.REQ);
		socket.connect("tcp://localhost:6006");

		for (String arg : args) {
			String url_path = MessageUtils.getUrlFromPath(arg);	
			String message = "URL;"+url_path;
			sendMesasge(message);
		}
	}

	/**
	 * FILE UPDATE DETECTED
	 */
	public void update(Observable o, Object arg) {
		File f = (File) arg;	
		String fqn = MessageUtils.getFQNfromPath(f.getAbsolutePath());
		
		try {
			String message = "RED;"+fqn+";"+f.getAbsolutePath()+";"+f.toURL().toString();
			sendMesasge(message);
		} catch (MalformedURLException e) {
			log.error(e);
		}
	}
	
	
	
	
	private static void sendMesasge(String message) {
		MessageUtils.displaymessage(message);
		socket.send(message);
		log.info("RESPONSE " + new String(socket.recv(0), ZMQ.CHARSET)+" [[END]]\r\n");
	}

}
