package com.groundstation.filemonitor;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.log4j.Logger;

import com.google.common.collect.Lists;

public class FileUpdateMonitor extends Observable {

	private static final Logger log = Logger.getLogger(FileUpdateMonitor.class);

	public void run( String[] args ) throws Exception {

		//Clear non directory
		List<String> list_targetclassRoots = Lists.newArrayList(args);
		filterNonDirectoryItems(list_targetclassRoots);

		//Monitor changes
		FileAlterationMonitor monitor = new FileAlterationMonitor(1000);
		FileAlterationListener listener = new FileAlterationListenerAdaptor() {
			@Override public void onFileCreate(File file) {
				log.debug("NOOP : File created:"+file);
			}
			@Override public void onFileDelete(File file) {
				log.debug("NOOP : File deleted:"+file);
			}
			@Override public void onFileChange(File file) {
				log.info("UPDATE detected : " + file);
				sendEvent(file);
			}
		};
		for (String directory : list_targetclassRoots) {
			FileAlterationObserver obs = new FileAlterationObserver(directory);
			obs.addListener(listener);
			monitor.addObserver(obs);
		}
		monitor.start();
	}

	
	
	
	
	private  void filterNonDirectoryItems(List<String> liste) {
		Iterator<String> it = liste.iterator();
		log.info(liste.size()+" items");
		//TODO Filter valid url
//		while (it.hasNext()) {
//			String directory = it.next();
//			try {
//				if (!Files.isDirectory(Paths.get(new URI(directory)))) {
//					log.warn("Directory is not properly formated " + directory);
//					it.remove();
//				} else {
//					log.info("MONITORING" + directory);
//				}
//			} catch (URISyntaxException x) {
//				log.error(x);
//			}
//		}
	}

	private  void sendEvent(Object o) {
		this.setChanged();
		notifyObservers(o);
	}

}
