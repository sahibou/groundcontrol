package com.groundstation.utils;

import java.util.Arrays;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.zeromq.ZMQ;

public class MessageUtils {

	private static Logger log = Logger.getLogger(MessageUtils.class);
	private static String splitcar = ";";
	
	public static String getFQNfromPath(String path) {
		String fqn=path;
		fqn=fqn.replace("\\", ".");
		fqn=fqn.substring(fqn.indexOf("target.classes")+"target.classes".length()+1);
		fqn=fqn.replace(".class", "");
		return fqn;
	}
	
	public static String getUrlFromPath(String windows_path) {
		String url_path= windows_path;
		url_path=url_path.replace("\\", "/");
		url_path="file:/"+url_path;
		if(!url_path.endsWith("/"))
			url_path+="/";//see https://docs.oracle.com/javase/7/docs/api/java/net/URLClassLoader.html		
		return url_path;
	}
	
	public static void displaymessage(String message) {
		int i = 0;
		log.info("------------------------- NEW MESSAGE :");
		Iterator<String> it = Arrays.asList(message.split(splitcar)).iterator();
		while(it.hasNext())
			log.info("["+(i++)+"]\t("+it.next()+")");
	}
	

}
