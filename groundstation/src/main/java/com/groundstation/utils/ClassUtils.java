package com.groundstation.utils;

import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class ClassUtils {
	private static final Logger log = Logger.getLogger(ClassUtils.class);
	private static URLClassLoader url_classloader = null;
	
	public ClassUtils(String[] paths) {
		URL[] urls =new URL[paths.length];
		for(int i=0; i<paths.length; i++)
			try {
				urls[i]=new URL(paths[i]);
			} catch (MalformedURLException x) {
				log.error(x);
			}
		url_classloader = new URLClassLoader(urls);
	}
	
	public Class<?> getClassFromFile(String filename) {
		if(filename.indexOf("target\\classes") == -1) {
			log.error("Hmm, doesn't seem like the file is part of a maven build ..."+filename);
			return null;
		}
		if(!filename.endsWith(".class")) {
			log.error("Hmm, not a class file ..."+filename);
			return null;
		}
		
		//Class FQ name
		int splitindex = filename.indexOf("target\\classes")+"target\\classes".length();
		String fn2 = filename.substring(splitindex+1);//+1 pour le 1er separateur
		String class_fq_name = StringUtils.replace(fn2, "\\", ".");
		class_fq_name=StringUtils.replace(class_fq_name, ".class", "");
		log.info("CLASS ---   "+class_fq_name);
		
		try {
			return url_classloader.loadClass(class_fq_name);
		} catch (ClassNotFoundException e) {
			log.error("Error loading class "+class_fq_name);
			log.error(e);
			return null;
		}
	}
	
	public byte[] getClassBytes(File f) {
		FileInputStream fs;
		try {
			fs = new FileInputStream(f);
			byte[] content = new byte[fs.available()];
			fs.read(content);
			fs.close();
			return content;
		} catch (Exception e) {
			log.error(e);
			return null;
		}
		
	}
}
